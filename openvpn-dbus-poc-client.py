#!/usr/bin/python2
#
#  Simple and stupid Proof-of-Concept D-Bus client for OpenVPN
#  This needs ovpn-mngr-server.py to run
#
#  Copyright (C) 2016   David Sommerseth <davids@openvpn.net>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
#    !!!!!   THIS IS PROOF-OF-CONCEPT CODE   !!!!!
#
#  That means this is NOT intended for production usage at all.
#  This code is not pretty, it is hacky and it will have bugs
#
#  HOW TO TEST THIS:   See instructions in openvpn-dbus-poc-server.py
#

import sys
import dbus

# Get connected to the D-Bus, using the session bus currently
bus = dbus.SessionBus()

# First get access to an object on the D-Bus, then call a method.
# The result should be fairly obvious.
managementobj = bus.get_object("net.openvpn.management", "/net/openvpn/management")
print "OpenVPN server PID: %i" % managementobj.GetPID()

# Lets retrieve some more generic info.  As the method we are calling is
# located under a different interface, make our object aware of that.
srv_status = managementobj.GetStatus(dbus_interface="net.openvpn.management.server")

# ... and profit
print "Status retrieved: %s" % srv_status["status_timestamp"]
print "OpenVPN version: %s (build: %s)" % (srv_status["version"], srv_status["build"])
print "Uptime: %s" % srv_status["uptime"]
print

#
# Retrieve information about currently connected clients
#
print "##########################"
print "###   ACTIVE SESSION   ###"
print "##########################"
print

# Loop through all active sessions
for sessid in managementobj.GetActiveSessions(dbus_interface="net.openvpn.management.server"):

    # Retrieve an object for this particular session
    objpath = "/net/openvpn/management/%s" % sessid
    sessionobj = bus.get_object("net.openvpn.management", objpath)
    sessinfo = sessionobj.GetSessionInfo(dbus_interface="net.openvpn.management.sessions")

    cn = sessinfo["Common Name"] # Just for simplicity below
    print "===== [ Common Name: %s ] %s" % (cn, "="*(54-len(cn)))
    print " Object path: %s" % objpath

    # Just do a simple dump of all data we've received
    for k, v in sorted(sessinfo.iteritems()):
        if k not in ("sessid","Common Name"): # Lets filter out a couple of elements
            print " %-25.25s: %s" % (k, v)

    # Retrieve routing information for this session
    print
    print "   -- [ Routing ] %s" % ("-"*60)
    print "    Last_reference          Virtual_address"
    for route in sessionobj.GetRouting():
        print "    %-22.22s  %s" % (route["Last Ref"], route["Virtual Address"])
    print


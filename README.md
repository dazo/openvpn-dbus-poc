OpenVPN D-Bus Proof-of-Concept
==============================

This is just some very simple and stupid proof of concept code which demonstrates
how OpenVPN can benefit of using D-Bus for its management interface.

This PoC is right now just acting as a bridge between the TCP based management
interface of OpenVPN.  This itself does not bring too much to the table.  But
adding more advanced features, such as D-Bus signaling ("push" notifications to
D-Bus clients on certiain events), will require far more work on proxy.

In this repository you will find a server side component which must run
first.  This component is the core bridge between the TCP based management
interface and the D-Bus.  When this service is running, any D-Bus compliant
client will be able to query (introspect) the interfaces provided by this
proxy.

In addition there is a simple client side component which extracts various
information on the server and presents it nicely on the console.

For more information, look at the instructions in openvpn-dbus-poc-server.py


#!/usr/bin/perl
#
#  Simple and stupid Proof-of-Concept D-Bus client for OpenVPN
#  This needs ovpn-mngr-server.py to run
#
#  Copyright (C) 2016   David Sommerseth <davids@openvpn.net>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
#    !!!!!   THIS IS PROOF-OF-CONCEPT CODE   !!!!!
#
#  That means this is NOT intended for production usage at all.
#  This code is not pretty, it is hacky and it will have bugs
#
#  HOW TO TEST THIS:   See instructions in openvpn-dbus-poc-server.py
#

use warnings;
use strict;
use Net::DBus;

# Connect to the D-Bus session bus and to the OpenVPN management service
my $bus = Net::DBus->session();
my $service = $bus->get_service("net.openvpn.management");

# Get the management object
my $managmnt = $service->get_object("/net/openvpn/management");

# Pretty obvious calls to the management object...
printf "OpenVPN server pid: %i\n", $managmnt->GetPID();

my $status = $managmnt->GetStatus();
printf "Status retrieved: %s\n", $status->{"status_timestamp"};
printf "Version: %s (build %s)\n", $status->{"version"}, $status->{"build"};
printf "Uptime: %s\n\n", $status->{"uptime"};

printf "###########################\n";
printf "###   ACTIVE SESSIONS   ###\n";
printf "###########################\n\n";

# Loop through all active session
foreach my $sessid (@{$managmnt->GetActiveSessions()}) {
    # Get the session object for a particular session ID ...
    my $sessobj = $managmnt->get_child_object("/${sessid}");

    # ... and parse the information
    my $sessinfo = $sessobj->GetSessionInfo();
    printf "===== [ Common Name: %s] ===============\n", $sessinfo->{"Common Name"};
    printf "Object path: /net/openvpn/managment/%s\n", ${sessid};
    printf "Bytes received:    %s\n", $sessinfo->{"Bytes Received"};
    printf "Bytes sent:        %s\n", $sessinfo->{"Bytes Sent"};
    printf "Connected since:   %s\n", $sessinfo->{"Connected Since"};
    printf "Real address:      %s\n", $sessinfo->{"Real Address"};
    printf "Username:          %s\n", $sessinfo->{"Username"};
    printf "Virtual address:   %s\n\n", $sessinfo->{"Virtual Address"};

    # Extract routing details as well
    printf "   -- [ Routing ] ------------------------------\n";
    printf "    Last_reference          Virtual_address\n";
    foreach my $route (@{$sessobj->GetRouting()}) {
        printf "    %-22.22s  %s\n", $route->{"Last Ref"}, $route->{"Virtual Address"};
    }
    print "\n"
}


